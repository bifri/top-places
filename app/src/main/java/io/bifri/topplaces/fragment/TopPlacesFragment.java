package io.bifri.topplaces.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import io.bifri.topplaces.App;
import io.bifri.topplaces.R;
import io.bifri.topplaces.util.ApplicationInstrumentation;
import io.bifri.topplaces.view.TopPlacesView;
import io.bifri.topplaces.view.TopPlacesViewBinder;
import io.bifri.topplaces.viewmodel.TopPlacesViewModel;

public class TopPlacesFragment extends Fragment {

    private TopPlacesView mTopPlacesView;
    private TopPlacesViewBinder mTopPlacesViewBinder;

    @Inject
    TopPlacesViewModel mTopPlacesViewModel;
    @Inject ApplicationInstrumentation mInstrumentation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getInstance().getGraph().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.top_locations_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTopPlacesView = new TopPlacesView(getContext(), view);
        mTopPlacesViewBinder = new TopPlacesViewBinder(mTopPlacesView, mTopPlacesViewModel);
    }

    @Override
    public void onResume() {
        super.onResume();
        mTopPlacesViewModel.subscribeToDataStore();
        mTopPlacesViewBinder.bind();
    }

    @Override
    public void onPause() {
        mTopPlacesViewBinder.unbind();
        mTopPlacesViewModel.unsubscribeFromDataStore();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        mTopPlacesView.onDestroyView();
        mTopPlacesView = null;
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        mTopPlacesViewModel.dispose();
        mInstrumentation.getLeakTracing().traceLeakage(this);
        super.onDestroy();
    }

}
