/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.topplaces.injection;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Component;
import io.bifri.topplaces.App;
import io.bifri.topplaces.activity.TopPlacesActivity;
import io.bifri.topplaces.data.DataStoreModule;
import io.bifri.topplaces.fragment.TopPlacesFragment;
import io.bifri.topplaces.location.LocationServicesModule;
import io.bifri.topplaces.viewmodel.TopPlacesViewModel;
import io.bifri.topplaces.viewmodel.ViewModelModule;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        DataStoreModule.class,
        LocationServicesModule.class,
        ViewModelModule.class,
        InstrumentationModule.class
})
public interface Graph {

    void inject(App app);
    void inject(TopPlacesActivity topPlacesActivity);
    void inject(TopPlacesFragment topPlacesFragment);
    void inject(TopPlacesViewModel topPlacesViewModel);

    final class Initializer {

        public static Graph init(Application application) {
            return DaggerGraph.builder()
                              .applicationModule(new ApplicationModule(application))
                              .build();
        }
    }

}
