
package io.bifri.topplaces.network.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlaceResponse {

    @SerializedName("valsts")
    @Expose
    private String valsts;
    @SerializedName("admin_vien")
    @Expose
    private String adminVien;
    @SerializedName("terit_vien")
    @Expose
    private String teritVien;
    @SerializedName("apdz_vieta")
    @Expose
    private String apdzVieta;
    @SerializedName("iela")
    @Expose
    private String iela;
    @SerializedName("maja")
    @Expose
    private String maja;
    @SerializedName("index")
    @Expose
    private String index;
    @SerializedName("korpuss")
    @Expose
    private String korpuss;
    @SerializedName("vzd_id")
    @Expose
    private String vzdId;
    @SerializedName("distance")
    @Expose
    private Integer distance;
    @SerializedName("x")
    @Expose
    private String x;
    @SerializedName("y")
    @Expose
    private String y;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lon")
    @Expose
    private Double lon;
    @SerializedName("suggestion")
    @Expose
    private String suggestion;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("iso_code")
    @Expose
    private String isoCode;
    @SerializedName("dist")
    @Expose
    private Integer dist;
    @SerializedName("country_name")
    @Expose
    private String countryName;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PlaceResponse() {
    }

    /**
     * 
     * @param countryName
     * @param lon
     * @param index
     * @param vzdId
     * @param isoCode
     * @param iela
     * @param valsts
     * @param distance
     * @param adminVien
     * @param name
     * @param teritVien
     * @param apdzVieta
     * @param suggestion
     * @param maja
     * @param korpuss
     * @param lat
     * @param y
     * @param dist
     * @param x
     */
    public PlaceResponse(String valsts, String adminVien, String teritVien, String apdzVieta, String iela, String maja, String index, String korpuss, String vzdId, Integer distance, String x, String y, Double lat, Double lon, String suggestion, String name, String isoCode, Integer dist, String countryName) {
        super();
        this.valsts = valsts;
        this.adminVien = adminVien;
        this.teritVien = teritVien;
        this.apdzVieta = apdzVieta;
        this.iela = iela;
        this.maja = maja;
        this.index = index;
        this.korpuss = korpuss;
        this.vzdId = vzdId;
        this.distance = distance;
        this.x = x;
        this.y = y;
        this.lat = lat;
        this.lon = lon;
        this.suggestion = suggestion;
        this.name = name;
        this.isoCode = isoCode;
        this.dist = dist;
        this.countryName = countryName;
    }

    public String getValsts() {
        return valsts;
    }

    public void setValsts(String valsts) {
        this.valsts = valsts;
    }

    public String getAdminVien() {
        return adminVien;
    }

    public void setAdminVien(String adminVien) {
        this.adminVien = adminVien;
    }

    public String getTeritVien() {
        return teritVien;
    }

    public void setTeritVien(String teritVien) {
        this.teritVien = teritVien;
    }

    public String getApdzVieta() {
        return apdzVieta;
    }

    public void setApdzVieta(String apdzVieta) {
        this.apdzVieta = apdzVieta;
    }

    public String getIela() {
        return iela;
    }

    public void setIela(String iela) {
        this.iela = iela;
    }

    public String getMaja() {
        return maja;
    }

    public void setMaja(String maja) {
        this.maja = maja;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getKorpuss() {
        return korpuss;
    }

    public void setKorpuss(String korpuss) {
        this.korpuss = korpuss;
    }

    public String getVzdId() {
        return vzdId;
    }

    public void setVzdId(String vzdId) {
        this.vzdId = vzdId;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public Integer getDist() {
        return dist;
    }

    public void setDist(Integer dist) {
        this.dist = dist;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

}
