package io.bifri.topplaces.network;

import android.support.annotation.NonNull;

import java.util.List;

import io.bifri.topplaces.network.pojo.response.PlaceResponse;
import io.bifri.topplaces.util.rx.DelayStrategy;
import io.bifri.topplaces.util.rx.RetryWithDelay;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

import static io.bifri.topplaces.util.rx.DelayStrategy.CONSTANT_DELAY_TIMES_RETRY_COUNT;
import static io.bifri.topplaces.util.Preconditions.checkNotNull;
import static rx.schedulers.Schedulers.io;

public class NetworkApi {

    private static final int DEFAULT_RETRY_COUNT = 3;
    private static final int DEFAULT_RETRY_DELAY_MILLIS = 1000;
    private static final DelayStrategy DEFAULT_RETRY_DELAY_STRATEGY =
            CONSTANT_DELAY_TIMES_RETRY_COUNT;

    @NonNull
    private final MappostService mMappostService;

    public NetworkApi(@NonNull final OkHttpClient client) {
        checkNotNull(client, "Client cannot be null.");

        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(io()))
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(MappostService.BASE_URL)
                .client(client)
                .build();

        mMappostService = retrofit.create(MappostService.class);
    }

    @NonNull
    public Observable<List<PlaceResponse>> places(@NonNull Double lat, @NonNull Double lon) {
        return mMappostService.places(String.valueOf(lat) + "," + String.valueOf(lon))
                .retryWhen(new RetryWithDelay(
                        DEFAULT_RETRY_COUNT,
                        DEFAULT_RETRY_DELAY_MILLIS,
                        DEFAULT_RETRY_DELAY_STRATEGY));
    }

}
