package io.bifri.topplaces.network;

import java.util.List;

import io.bifri.topplaces.network.pojo.response.PlaceResponse;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface MappostService {

    String BASE_URL = "http://maps.kartes.lv/kijs/v3/DEMO/api/";

    @GET("./?method=reverse_geocoding&type=suggest_place")
    Observable<List<PlaceResponse>> places(@Query("latlon") String latLon);

}
