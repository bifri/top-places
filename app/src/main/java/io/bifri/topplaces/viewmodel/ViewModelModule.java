package io.bifri.topplaces.viewmodel;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import io.bifri.topplaces.data.DataStoreModule;
import io.bifri.topplaces.data.dao.PlaceDaoExt;
import io.bifri.topplaces.injection.ForApplication;
import io.bifri.topplaces.network.NetworkApi;
import io.bifri.topplaces.network.NetworkModule;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;

@Module(includes = { NetworkModule.class, DataStoreModule.class })
public class ViewModelModule {

    @Provides
    public TopPlacesViewModel provideTopPlacesViewModel(
            @ForApplication Context appContext,
            ReactiveLocationProvider locationProvider,
            NetworkApi networkApi,
            PlaceDaoExt placeDaoExt) {

        return new TopPlacesViewModel(appContext, locationProvider, networkApi, placeDaoExt);
    }

}
