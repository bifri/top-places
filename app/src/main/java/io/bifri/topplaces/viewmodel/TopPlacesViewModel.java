package io.bifri.topplaces.viewmodel;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.google.android.gms.location.LocationRequest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.bifri.topplaces.data.dao.PlaceDaoExt;
import io.bifri.topplaces.data.dataobject.Place;
import io.bifri.topplaces.data.dataobject.TopPlace;
import io.bifri.topplaces.network.NetworkApi;
import io.bifri.topplaces.util.Log;
import io.bifri.topplaces.util.rx.RxState;
import io.bifri.topplaces.util.rx.StartWith;
import java8.util.Optional;
import java8.util.stream.StreamSupport;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;
import rx.observables.ConnectableObservable;
import rx.subscriptions.CompositeSubscription;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.support.v4.app.ActivityCompat.checkSelfPermission;
import static android.text.format.DateUtils.MINUTE_IN_MILLIS;
import static io.bifri.topplaces.util.Preconditions.checkNotNull;
import static io.bifri.topplaces.util.Preconditions.get;
import static rx.android.schedulers.AndroidSchedulers.mainThread;
import static rx.schedulers.Schedulers.computation;
import static rx.schedulers.Schedulers.io;


public class TopPlacesViewModel extends AbstractViewModel {

    private static final String TAG = TopPlacesViewModel.class.getSimpleName();

    private static final int DEFAULT_PRIORITY = LocationRequest.PRIORITY_HIGH_ACCURACY;
    private static final long DEFAULT_INTERVAL_IN_MILLIS = MINUTE_IN_MILLIS;
    private static final int DEFAULT_DAYS_PERIOD = 7;
    private static final int DEFAULT_TOP_PLACES_LIMIT = 3;

    @NonNull private final Context mAppContext;

    @NonNull private final ReactiveLocationProvider mLocationProvider;
    @NonNull private final NetworkApi mNetworkApi;
    @NonNull private final PlaceDaoExt mPlaceDaoExt;
    @NonNull private final RxState<Optional<Place>> mOutStreamOfCurrentPlace =
            new RxState<>(Optional.empty(), mainThread());
    @NonNull private final RxState<Optional<List<TopPlace>>> mOutStreamOfTopPlaces =
            new RxState<>(Optional.empty(), mainThread());

    public TopPlacesViewModel(
            @NonNull final Context appContext,
            @NonNull final ReactiveLocationProvider locationProvider,
            @NonNull final NetworkApi networkApi,
            @NonNull final PlaceDaoExt placeDaoExt) {
        this.mAppContext = appContext;
        this.mLocationProvider = get(locationProvider);
        this.mNetworkApi = networkApi;
        this.mPlaceDaoExt = placeDaoExt;
    }

    @RxLogObservable
    public Observable<Place> getStreamOfCurrentPlace() {
        return mOutStreamOfCurrentPlace.values(StartWith.SCHEDULE)
                .filter(Optional::isPresent)
                .map(Optional::get);
    }

    @RxLogObservable
    public Observable<List<TopPlace>> getStreamOfTopPlaces() {
        return mOutStreamOfTopPlaces.values(StartWith.SCHEDULE)
                .filter(Optional::isPresent)
                .map(Optional::get);
    }

    @RxLogObservable
    private @NonNull Observable<Location> getCurrentLocationObservable() {
        if (checkSelfPermission(mAppContext, ACCESS_FINE_LOCATION) != PERMISSION_GRANTED
                && checkSelfPermission(mAppContext, ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return Observable.empty();
        }
        return mLocationProvider.getUpdatedLocation(locationRequest())
                .sample(DEFAULT_INTERVAL_IN_MILLIS, TimeUnit.MILLISECONDS);
    }

    private LocationRequest locationRequest() {
        return LocationRequest.create()
                .setPriority(DEFAULT_PRIORITY)
                .setInterval(DEFAULT_INTERVAL_IN_MILLIS);
    }

    @RxLogObservable
    private @NonNull Observable<Location> getLastKnownLocationObservable() {
        if (checkSelfPermission(mAppContext, ACCESS_FINE_LOCATION) != PERMISSION_GRANTED
                && checkSelfPermission(mAppContext, ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return Observable.empty();
        }
        return mLocationProvider.getLastKnownLocation();
    }

    @RxLogObservable
    private @NonNull Observable<Place> getPlaceObservable(@NonNull Double lat, @NonNull Double lon) {
        return mNetworkApi.places(lat, lon)
                .subscribeOn(io())
                .observeOn(computation())
                .map(places -> StreamSupport.stream(places)
                        .reduce((place, acc) ->
                                place.getDistance() < acc.getDistance()? place : acc))
                .filter(Optional::isPresent)
                .map(placeOptional -> placeOptional.get().getSuggestion())
                .filter(address -> !TextUtils.isEmpty(address))
                .map(address -> new Place(address.hashCode(), lat, lon, address, ""));
    }

    @Override
    public void subscribeToDataStoreInternal(@NonNull final CompositeSubscription compositeSubscription) {
        checkNotNull(compositeSubscription);
        Log.v(TAG, "subscribeToDataStoreInternal");

        ConnectableObservable<Place> placeConnectableObservable =
                getCurrentLocationObservable()
                        .subscribeOn(mainThread())
                        .observeOn(computation())
                        .flatMap(location -> getPlaceObservable(location.getLatitude(), location.getLongitude()))
                        .publish();

        compositeSubscription.add(placeConnectableObservable
                .startWith(getLastKnownLocationObservable()
                        .flatMap(location -> getPlaceObservable(location.getLatitude(), location.getLongitude())))
                .subscribe(place -> mOutStreamOfCurrentPlace.apply(__ -> Optional.of(place)),
                        e -> Log.e(TAG, e.getMessage(), e))
        );

        compositeSubscription.add(placeConnectableObservable
                .doOnNext(mPlaceDaoExt::savePlaceInTx)
                .switchMap(__ -> Observable.fromCallable(() ->
                        mPlaceDaoExt.topPlaces(DEFAULT_DAYS_PERIOD, DEFAULT_TOP_PLACES_LIMIT)))
                .startWith(Observable.fromCallable(() ->
                        mPlaceDaoExt.topPlaces(DEFAULT_DAYS_PERIOD, DEFAULT_TOP_PLACES_LIMIT)))
                .subscribe(topPlaces -> mOutStreamOfTopPlaces.apply(__ -> Optional.of(topPlaces)),
                        e -> Log.e(TAG, e.getMessage(), e))
        );

        compositeSubscription.add(placeConnectableObservable.connect());
    }

}
