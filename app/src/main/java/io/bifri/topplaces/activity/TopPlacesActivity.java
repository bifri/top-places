package io.bifri.topplaces.activity;

import android.Manifest;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.tbruyelle.rxpermissions.RxPermissions;

import io.bifri.topplaces.R;
import io.bifri.topplaces.fragment.TopPlacesFragment;
import io.bifri.topplaces.util.SdkUtils;

public class TopPlacesActivity extends AppCompatActivity {

    private static final String TAG = TopPlacesActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Pair<Boolean, String> pair = SdkUtils.checkGooglePlayServices(this);
        if (!pair.first) {
            Toast.makeText(this, R.string.play_services_problem, Toast.LENGTH_LONG)
                    .show();
            finish();
        }

        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions.setLogging(true);

        // Must be done during an initialization phase like onCreate
        rxPermissions
                .request(Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe(granted -> {
                    if (granted) { // Always true pre-M
                        Log.i(TAG, "All permissions granted.");
                        onCreateAfterAllPermissionsGranted(savedInstanceState);
                    } else {
                        Log.i(TAG, "Some or all permissions are not granted.");
                        Toast.makeText(this, R.string.app_stop_no_permissions, Toast.LENGTH_LONG)
                                .show();
                        finish();
                    }
                });
    }

    protected void onCreateAfterAllPermissionsGranted(Bundle savedInstanceState) {
        setContentView(R.layout.top_locations_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new TopPlacesFragment())
                    .commit();
        }
    }

}
