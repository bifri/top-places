package io.bifri.topplaces.adapter;

import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import io.bifri.topplaces.R;
import io.bifri.topplaces.adapter.viewholder.TopPlaceViewHolder;
import io.bifri.topplaces.data.dataobject.TopPlace;

import static android.view.View.OnClickListener;

public class TopPlacesAdapter extends Adapter<TopPlaceViewHolder> {

    private final List<TopPlace> mTopPlaces = new ArrayList<>();
    private OnClickListener mOnClickListener;

    public TopPlacesAdapter() {

    }

    public void setOnClickListener(OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    public TopPlace getItem(int position) {
        return mTopPlaces.get(position);
    }

    @Override
    public TopPlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.top_place_item, parent, false);
        return new TopPlaceViewHolder(v, mOnClickListener);
    }

    @Override
    public void onBindViewHolder(TopPlaceViewHolder holder, int position) {
        holder.bind(mTopPlaces.get(position));
    }

    @Override
    public int getItemCount() {
        return mTopPlaces.size();
    }

    public void set(List<TopPlace> topPlaces) {
        int prevSize = mTopPlaces.size();
        this.mTopPlaces.clear();
        this.mTopPlaces.addAll(topPlaces);
        if (prevSize > 0) {
            notifyItemRangeRemoved(0, prevSize);
        }
        notifyItemRangeInserted(0, topPlaces.size());
    }

}
