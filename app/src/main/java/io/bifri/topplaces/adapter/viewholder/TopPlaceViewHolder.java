package io.bifri.topplaces.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.format.DateUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.bifri.topplaces.R;
import io.bifri.topplaces.data.dataobject.TopPlace;

public class TopPlaceViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.top_place) TextView mTVTopPlace;

    public TopPlaceViewHolder(View view, OnClickListener onClickListener) {
        super(view);
        ButterKnife.bind(this, view);
        view.setOnClickListener(onClickListener);
    }

    public void bind(TopPlace topPlace) {
        CharSequence relativeTime = DateUtils.getRelativeTimeSpanString(topPlace.lastVisitDatetime());
        String address = topPlace.address();
        String topPlaceInfo = relativeTime + " " + address;

        Spannable spanText = new SpannableString(topPlaceInfo);
        spanText.setSpan(
                new ForegroundColorSpan(
                        mTVTopPlace.getContext().getResources().getColor(
                                android.R.color.holo_blue_dark)),
                0,
                relativeTime.length(),
                0);

        mTVTopPlace.setText(spanText);
    }

}
