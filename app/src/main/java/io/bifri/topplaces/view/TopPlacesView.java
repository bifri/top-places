package io.bifri.topplaces.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import hugo.weaving.DebugLog;
import io.bifri.topplaces.R;
import io.bifri.topplaces.adapter.TopPlacesAdapter;
import io.bifri.topplaces.data.dataobject.Place;
import io.bifri.topplaces.data.dataobject.TopPlace;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;

import static io.bifri.topplaces.util.Preconditions.checkNotNull;

public class TopPlacesView {

    @BindView(R.id.current_place) TextView mTVCurrentPlace;
    @BindView(R.id.top_places_list_view) RecyclerView mRVTopPlaces;
    private Unbinder mUnbinder;

    private TopPlacesAdapter mTopPlacesAdapter;

    public TopPlacesView(Context context, View rootView) {
        mUnbinder = ButterKnife.bind(this, rootView);

        mTopPlacesAdapter = new TopPlacesAdapter();
        mRVTopPlaces.setLayoutManager(new LinearLayoutManager(context));
        mRVTopPlaces.setAdapter(mTopPlacesAdapter);
        mRVTopPlaces.setHasFixedSize(true);
        mRVTopPlaces.setItemAnimator(new SlideInLeftAnimator());
        mRVTopPlaces.addItemDecoration(
                new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
    }

    public void onDestroyView() {
        mUnbinder.unbind();
    }

    @DebugLog
    void setCurrentPlace(@NonNull final Place place) {
        checkNotNull(place);

        mTVCurrentPlace.setText(place.getAddress());
    }

    @DebugLog
    void setTopPlaces(@NonNull final List<TopPlace> topPlaces) {
        checkNotNull(topPlaces);
        checkNotNull(mTopPlacesAdapter);

        mTopPlacesAdapter.set(topPlaces);
    }

}
