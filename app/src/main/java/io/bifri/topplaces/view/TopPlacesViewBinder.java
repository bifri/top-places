package io.bifri.topplaces.view;

import android.support.annotation.NonNull;

import io.bifri.topplaces.viewmodel.TopPlacesViewModel;
import rx.subscriptions.CompositeSubscription;

import static io.bifri.topplaces.util.Preconditions.get;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class TopPlacesViewBinder extends RxViewBinder {

    private final TopPlacesView view;
    private final TopPlacesViewModel viewModel;

    public TopPlacesViewBinder(@NonNull final TopPlacesView view,
                               @NonNull final TopPlacesViewModel viewModel) {
        this.view = get(view);
        this.viewModel = get(viewModel);
    }

    @Override
    protected void bindInternal(@NonNull final CompositeSubscription s) {
        s.add(viewModel.getStreamOfTopPlaces()
                .observeOn(mainThread())
                .subscribe(view::setTopPlaces));

        s.add(viewModel.getStreamOfCurrentPlace()
                .observeOn(mainThread())
                .subscribe(view::setCurrentPlace));
    }

}
