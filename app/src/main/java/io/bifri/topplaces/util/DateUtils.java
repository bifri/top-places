package io.bifri.topplaces.util;

import android.support.v4.util.Pair;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {

    public static final DateFormat ISO_DATETIME_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ROOT);
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.ROOT);

    public static final Date EMPTY_DATE = new Date(0);

    private DateUtils() {}

    public static Date value(Date date) {
        return date != null ? date : new Date();
    }

    public static long toDbValue(Date date) {
        return date != null
                ? date.getTime()
                : 0;
    }

    public static Date fromDbValue(long value) {
        return value > 0
                ? new Date(value)
                : null;
    }

    public static boolean isLater(Date first, Date second) {
        if (first == null) {
            return false;
        } else if (second == null) {
            return true;
        } else {
            return first.compareTo(second) > 0;
        }
    }

    public static Pair<Long, Long> range(int daysPeriodFromNow) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"), Locale.ROOT);
        long endRangeDate = cal.getTimeInMillis();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.DAY_OF_YEAR, -daysPeriodFromNow);
        long startRangeDate = cal.getTimeInMillis();
        return new Pair<>(startRangeDate, endRangeDate);
    }

}
