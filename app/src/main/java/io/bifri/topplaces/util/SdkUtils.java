package io.bifri.topplaces.util;

import android.content.Context;
import android.support.v4.util.Pair;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import io.bifri.topplaces.util.Log;

public class SdkUtils {

    private static String TAG = SdkUtils.class.getSimpleName();

    public static Pair<Boolean, String> checkGooglePlayServices(Context context) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        final int status = googleApiAvailability.isGooglePlayServicesAvailable(context);
        if (status != ConnectionResult.SUCCESS) {
            Log.e(TAG, googleApiAvailability.getErrorString(status));
            return new Pair<>(false, googleApiAvailability.getErrorString(status));
        } else {
            return new Pair<>(true, null);
        }
    }

}
