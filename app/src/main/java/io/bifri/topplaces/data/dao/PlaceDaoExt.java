package io.bifri.topplaces.data.dao;


import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.bifri.topplaces.data.dataobject.DaoSession;
import io.bifri.topplaces.data.dataobject.Place;
import io.bifri.topplaces.data.dataobject.PlaceDao;
import io.bifri.topplaces.data.dataobject.PlaceDatetime;
import io.bifri.topplaces.data.dataobject.PlaceDatetimeDao;
import io.bifri.topplaces.data.dataobject.TopPlace;
import io.bifri.topplaces.util.DateUtils;
import io.bifri.topplaces.util.Log;

import static io.bifri.topplaces.util.DateUtils.ISO_DATETIME_FORMAT;

public class PlaceDaoExt {

    private static final String TAG = PlaceDaoExt.class.getSimpleName();

    @NonNull private final DaoSession mDaoSession;

    public PlaceDaoExt(@NonNull DaoSession daoSession) {
        mDaoSession = daoSession;
    }

    public void savePlaceInTx(Place place) {
        mDaoSession.runInTx(() -> savePlace(place));
    }

    private void savePlace(Place place) {
        Date now = new Date();
        place.setUpdatedAt(ISO_DATETIME_FORMAT.format(now));

        Place prevPlace = mDaoSession.getPlaceDao().load(place.getId());
        if (prevPlace != null) {
            mDaoSession.getPlaceDao().update(place);
        } else {
            mDaoSession.getPlaceDao().insert(place);
        }
        Log.d(TAG, "Saved: " + place);
        savePlaceDatetime(place, now);
    }

    private void savePlaceDatetime(Place place, Date now) {
        PlaceDatetime placeDatetime = new PlaceDatetime(place.getId(), now.getTime());
        mDaoSession.getPlaceDatetimeDao().insert(placeDatetime);
        Log.d(TAG, "Saved: " + placeDatetime);
    }

    public List<TopPlace> topPlaces(int daysPeriod, int placesCount) {
        List<TopPlace> result = new ArrayList<>();

        Pair<Long, Long> dateRange = DateUtils.range(daysPeriod);

        String selectPlaceIdAndVisitTimes =
                "(SELECT " + PlaceDatetimeDao.Properties.PlaceId.columnName + ", count(*) AS visit_count "
                        + "FROM " + PlaceDatetimeDao.TABLENAME + " "
                        + "GROUP BY " + PlaceDatetimeDao.Properties.PlaceId.columnName + " "
                        + "HAVING " + PlaceDatetimeDao.Properties.VisitDatetime.columnName + " BETWEEN ? AND ? "
                        + "ORDER BY visit_count DESC) AS vt";

        String selectLastVisitTime =
                "(SELECT " + PlaceDatetimeDao.Properties.PlaceId.columnName
                        + ", MAX(" + PlaceDatetimeDao.Properties.VisitDatetime.columnName + ") AS last_visit_datetime "
                        + "FROM " + PlaceDatetimeDao.TABLENAME + " "
                        + "GROUP BY " + PlaceDatetimeDao.Properties.PlaceId.columnName + ") "
                        + "AS lt";

        String select =
                "SELECT pl." + PlaceDao.Properties.Id.columnName + ", " +
                        "pl." + PlaceDao.Properties.Lat.columnName + ", " +
                        "pl." + PlaceDao.Properties.Lon.columnName + ", " +
                        "pl." + PlaceDao.Properties.Address.columnName + ", " +
                        "vt.visit_count, " +
                        "lt.last_visit_datetime "
                        + "FROM " + selectPlaceIdAndVisitTimes + " "
                        + "JOIN " + selectLastVisitTime + " ON vt." + PlaceDatetimeDao.Properties.PlaceId.columnName + " = lt." + PlaceDatetimeDao.Properties.PlaceId.columnName + " "
                        + "JOIN " + PlaceDao.TABLENAME + " pl ON pl." + PlaceDao.Properties.Id.columnName + " = vt." + PlaceDatetimeDao.Properties.PlaceId.columnName + " "
                        + "LIMIT " + placesCount;

        Cursor cursor = mDaoSession.getDatabase().rawQuery(
                select,
                new String[] { String.valueOf(dateRange.first), String.valueOf(dateRange.second) });
        if (cursor == null) {
            return new ArrayList<>();
        }
        try {
            while (cursor.moveToNext()) {
                TopPlace topPlace = new TopPlace(
                        cursor.getLong(0),
                        cursor.getDouble(1),
                        cursor.getDouble(2),
                        cursor.getString(3),
                        cursor.getInt(4),
                        cursor.getLong(5));
                result.add(topPlace);
            }
        } finally {
            cursor.close();
        }
        return result;
    }

}
