package io.bifri.topplaces.data;

import android.content.Context;

import org.greenrobot.greendao.database.Database;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.bifri.topplaces.data.dao.PlaceDaoExt;
import io.bifri.topplaces.data.dataobject.DaoMaster;
import io.bifri.topplaces.data.dataobject.DaoSession;
import io.bifri.topplaces.injection.ForApplication;

@Module
public final class DataStoreModule {

    private static final String DB_NAME = "app-db";
    private static final String DB_NAME_ENCRYPTED = "app-db-encrypted";
    private static final String ENCRYPTED_DB_PASSWORD = "super-secret";

    @Provides
    @Singleton
    @Named("encryptedDb")
    public boolean provideEncrypted() {
        return false;
    }

    @Provides
    @Singleton
    public DaoSession provideDaoSession(
            @ForApplication Context context,
            @Named("encryptedDb") boolean encrypted) {
        DbOpenHelper helper = new DbOpenHelper(
                context,
                encrypted ? DB_NAME_ENCRYPTED : DB_NAME);
        Database db = encrypted
                ? helper.getEncryptedWritableDb(ENCRYPTED_DB_PASSWORD)
                : helper.getWritableDb();
        return new DaoMaster(db).newSession();
    }

    @Provides
    @Singleton
    public PlaceDaoExt providePlaceDaoExt(DaoSession daoSession) {
        return new PlaceDaoExt(daoSession);
    }

}
