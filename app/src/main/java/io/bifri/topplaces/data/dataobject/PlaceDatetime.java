package io.bifri.topplaces.data.dataobject;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;


@Entity(
        nameInDb = "place_datetimes"
)
public class PlaceDatetime {

    @Id(autoincrement = true)
    private Long id;

    @Index
    @NotNull
    private long placeId;

    @NotNull
    private long visitDatetime;

    public PlaceDatetime() {

    }

    public PlaceDatetime(long placeId, long visitDatetime) {
        this.placeId = placeId;
        this.visitDatetime = visitDatetime;
    }

    @Generated(hash = 1011673768)
    public PlaceDatetime(Long id, long placeId, long visitDatetime) {
        this.id = id;
        this.placeId = placeId;
        this.visitDatetime = visitDatetime;
    }

    @Override
    public String toString() {
        return "PlaceDatetimes{" +
                "id=" + id +
                ", placeId=" + placeId +
                ", visitDatetime=" + visitDatetime +
                '}';
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getPlaceId() {
        return this.placeId;
    }

    public void setPlaceId(long placeId) {
        this.placeId = placeId;
    }

    public long getVisitDatetime() {
        return this.visitDatetime;
    }

    public void setVisitDatetime(long visitDatetime) {
        this.visitDatetime = visitDatetime;
    }
}
