package io.bifri.topplaces.data.dataobject;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;


@Entity(
        nameInDb = "places"
)
public class Place {

    @Id
    @NotNull
    private long id;

    @NotNull
    private double lat;

    @NotNull
    private double lon;

    @NotNull
    private String address;

    @NotNull
    private String updatedAt;

    public Place() {

    }

    @Generated(hash = 1457964764)
    public Place(long id, double lat, double lon, @NotNull String address,
            @NotNull String updatedAt) {
        this.id = id;
        this.lat = lat;
        this.lon = lon;
        this.address = address;
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "Place{" +
                "id=" + id +
                ", lat=" + lat +
                ", lon=" + lon +
                ", address='" + address + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                '}';
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getLat() {
        return this.lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return this.lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getUpdatedAt() {
        return this.updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
