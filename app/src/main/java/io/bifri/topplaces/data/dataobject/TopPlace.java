package io.bifri.topplaces.data.dataobject;

import android.support.annotation.NonNull;

public class TopPlace {

    @NonNull private final long id;
    @NonNull private final double lat;
    @NonNull private final double lon;
    @NonNull private final String address;
    @NonNull private final int visitCount;
    @NonNull private final long lastVisitDatetime;

    public TopPlace(@NonNull long id, @NonNull double lat, @NonNull double lon,
                    @NonNull String address, @NonNull int visitCount,
                    @NonNull long lastVisitDatetime) {
        this.id = id;
        this.lat = lat;
        this.lon = lon;
        this.address = address;
        this.visitCount = visitCount;
        this.lastVisitDatetime = lastVisitDatetime;
    }

    @NonNull
    public long id() {
        return id;
    }

    @NonNull
    public double lat() {
        return lat;
    }

    @NonNull
    public double lon() {
        return lon;
    }

    @NonNull
    public String address() {
        return address;
    }

    @NonNull
    public int visitCount() {
        return visitCount;
    }

    @NonNull
    public long lastVisitDatetime() {
        return lastVisitDatetime;
    }
}
