The task:
===================================================
* Write the program that shows where the user most often was in the last 7 days.
* When the application is started, every 60 seconds, the current geographic coordinates are requested and the current location is determined using the Mappost API.

Demo:
===============================================
* Screencast: https://drive.google.com/open?id=0B3xpzS7S85MyeUZnQ3JySXUxT3c
* Apk: https://drive.google.com/open?id=0B3xpzS7S85MyTF8wczBieWU4V2M

License
=======

    The MIT License

    Copyright (c) 2013-2016 reark project contributors

    https://github.com/reark/reark/graphs/contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.